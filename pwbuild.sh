#!/bin/bash
#Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
#SPDX-License-Identifier: BSD-3-Clause-Clear

usage(){
   cat <<'END_OF_USAGE'

Options:
    -h, --help
        Display this help list

Arguments:
    -b, --build_root
        Build Root Path

    -t, --target
        Target Product name

    Flags:
    -k, --compile_kernel
        Compile the kernel and create the boot image
    -u, --compile_userspace
        Compile the SRPMS and the SRPMS to their respective RPMS
    -s, --compile_simg
        Compile the system image using the userspace-rpms
    -a, --all  (Enabled by default if no othr option is selected)
	Compile all (Wont create system image)
    -c, --clean
         Clean up after the Build(delete rpmbuild/* ,kernel_rpms ,osbuild-manifests/_build)
    
     Ex:
    -->Compile Kernel and Userspace
        sh -x build.sh -b workspacepath -t sa8775
    -->Compile simg (Assumption: We have userspace-rpms and kernel_rpms folders in workspace_root)
        sh -x build.sh -s -b workspacepath -t sa8775
    -->Compile only kernel
        sh -x build.sh -k -b workspacepath -t sa8775
    -->Compile only userspace
        sh -x build.sh -u -b workspacepath -t sa8775
    -->Compile userspace and simg (Assumption: We have kernel_rpms folder in workspace_root)
        sh -x build.sh -u -s -b workspacepath -t sa8775
    -->Compile only simg and kernel (Assumption: We have userspace-rpms folder in workspace_root)
        sh -x build.sh -k -s -b workspacepath -t sa8775
    -->Cleanup (Cleans rpmbuild, kernel_rpms, userspace-rpms, system image)
        sh -x build.sh -c -b workspacepath -t sa8775

END_OF_USAGE

exit 1;
}



check_exit(){

ret=$?
if [[ $ret -ne 0 ]]; then            #If command exited with not 0 then print $1 string and exit
    echo -e "$1"
    echo "exit value $ret"
    exit $ret
fi
}

check_exit_with_retry() {

ret=$?
local mycommand="$@"
local retries=2
if [[ $ret -ne 0 ]]; then
     for i in $(seq $retries)
     do
         FLAG=0
         $mycommand
         if [ $? -eq 0 ]; then
              break
         else
              FLAG=1
         fi
     done
     if [[ $FLAG -eq 1 ]]; then            #If command exited with not 0 then print $1 string and exit
           echo -e "$mycommand"
	   echo "exit value $ret"
           exit $ret
     fi
fi
}

VALID_ARGS=$(getopt -o b:t:p:x:hkusadc --long build_root:,target:,project_conf:,dep_data:,help,compile_kernel,compile_userspace,compile_simg,all,superclean,clean -- "$@")

eval set -- "$VALID_ARGS"

kernel_flag=false
userspace_flag=false
simg_flag=false
all_flag=false
superclean=false
clean=false

while true; do
    case "$1" in
        -b|--build_root) BUILD_ROOT="$2"; shift;;
        -t|--target) TARGET="$2"; shift;;
        -p|--project_conf) project_conf="$2"; shift;;
        -x|--dep_data) dep_data="$2"; shift;;
	-h|--help) usage; exit 0;;
        -k|--compile_kernel) kernel_flag=true;;
        -u|--compile_userspace) userspace_flag=true;;
        -s|--compile_simg) simg_flag=true;;
        -a|--all) all_flag=true;;
	-d| --superclean) superclean=true;;
        -c| --clean) clean=true;;
        --) shift; break;;
        *)
            echo -e "Unrecognised option $1 \n $@" >&2
            usage
            ;;
    esac
    shift
done

if [[ "$kernel_flag" == "false" && "$userspace_flag" == "false" && "$simg_flag" == "false" && "$all_flag" == "false" && "$superclean" == "false" && "$clean" == "false" ]]; then
    all_flag="true"
fi


all_params=("BUILD_ROOT" "TARGET")
all_targets=("sa8540" "sa8775")

for para in ${all_params[@]}; do
    echo -e "$para = ${!para}"
    if [ -z "${!para}" ];then
        echo "value of parameter $para not found"  1>&2;
        exit 1
    fi
done

BUILD_ROOT="$(sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'<<<"${BUILD_ROOT}")" #Trimming the leading and trailing whitespaces

BUILD_ROOT=$(realpath "$BUILD_ROOT")

echo "absolute build root is $BUILD_ROOT"
if [ ! -d "$BUILD_ROOT" ];
then
	echo "$BUILD_ROOT directory is not valid, please enter valid directory and try again" 1>&2;
	exit 1
fi


TARGET="$(sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'<<<"${TARGET}")"   #Trimming the leading and trailing whitespaces


if ! printf '%s\0' "${all_targets[@]}" | grep -Fxqz -- ${TARGET}; then
	echo "Not a valid target, the valid targets are ${all_targets[@]}" 1>&2;
	exit 1
fi

#BUILD_ROOT="/local/mnt/workspace/lnxbuild/project/LRH_AU_ed6084fe-dfe9-f185-a3f9-3cecef3fc458"

RPM_ROOT="${BUILD_ROOT}/rpmbuild"
SOURCES="${RPM_ROOT}/SOURCES"
SRPMS="${RPM_ROOT}/SRPMS"
RPMS="${RPM_ROOT}/RPMS"
SPECS="${RPM_ROOT}/SPECS"
LOGS="${RPM_ROOT}/LOGS"
BTYPE=""
PROJ_CONF="${BUILD_ROOT}/$dep_data"
PATCH_SCRIPTS="$(dirname $PROJ_CONF)"
BOARD_CONF="${BUILD_ROOT}/$project_conf"
MANIFEST="${BUILD_ROOT}/.repo/manifest.xml"

echo "Enter sudo password"
sudo pwd
mkdir -p ${BUILD_ROOT}/rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS,LOGS}
trap "sudo rm -rf ${BUILD_ROOT}/rpmbuild ${BUILD_ROOT}/machine_root" INT QUIT EXIT

export RPMBUILD_DIR="$BUILD_ROOT/rpmbuild"
export GIT_SSH_COMMAND='ssh -o KexAlgorithms=+diffie-hellman-group1-sha1'

cd $BUILD_ROOT

mkdir -p $BUILD_ROOT/final_images
rm -rf $LOGS/tarlogs 

export MOCK_ROOT="$BUILD_ROOT/rpmbuild/BUILDROOT"
export MACHINE_ROOT="$BUILD_ROOT/machine_root"
export MOCK_CONFIG=/etc/mock/centos-stream+epel-9-aarch64.cfg

create_bootimage() {

echo "Starting boot image creation"
rm -rf $BUILD_ROOT/final_images/sa8775p-boot-5.14.img
cd $MOCK_ROOT/lib/modules
cd $(ls | grep "5.14.0-999.124ES.test.el9.aarch64" | grep -v debug)

if [ $TARGET == "sa8540" ]
then
     cat vmlinuz dtb/qcom/sa8540p-adp-ride.dtb > $BUILD_ROOT/final_images/vmlinuz-dtb
     ${BUILD_ROOT}/mkbootimg/mkbootimg.py --output $BUILD_ROOT/final_images/${TARGET}p-boot-5.14.img \
     --kernel $BUILD_ROOT/final_images/vmlinuz-dtb \
     --pagesize 4096 \
     --kernel_offset 0x80208000 \
     --second_offset 0x81100000 \
     --tags_offset 0x7d00000 \
     --base 0x1208800 \
     --cmdline "console=ttyMSM0,115200,n8 no_console_suspend=1 \
     earlycon=qcom_geni,0x884000 ignore_loglevel firmware_class.path=/firmware"
elif [ $TARGET == "sa8775" ]
then
       cat vmlinuz dtb/qcom/lemans.dtb > $BUILD_ROOT/final_images/vmlinuz-dtb
       ${BUILD_ROOT}/mkbootimg/mkbootimg.py --output $BUILD_ROOT/final_images/sa8775p-boot-5.14.img --kernel $BUILD_ROOT/final_images/vmlinuz-dtb --pagesize 4096 --kernel_offset 0x80208000 --second_offset 0x81100000 --tags_offset 0x7d00000 --base 0x1208800 --cmdline "console=ttyMSM0,115200,n8 no_console_suspend=1 earlycon=qcom_geni,0xa8c000 ignore_loglevel firmware_class.path=/firmware"
fi

check_exit "building boot image failed"
}


kernel_compile() {

echo "Starting fresh kernel compilation"

rm -rf $BUILD_ROOT/kernel_rpms
cd kernel/rh-kernel-5.14

sed -i 's,RHEL_RELEASE = 162,RHEL_RELEASE = 999,g' Makefile.rhelver
sed -i 's,AUTOMOTIVEBUILD:=.124.*,AUTOMOTIVEBUILD:=.124ES,g' Makefile.rhelver

make dist-srpm
KSRPM="$(basename redhat/rpm/SRPMS/kernel-automotive-*.src.rpm)"
mv redhat/rpm/SRPMS/kernel-automotive-*.src.rpm $SRPMS
mv redhat/rpm/SPECS/kernel.spec $SPECS

cd $RPM_ROOT
mock -r $MOCK_CONFIG --no-cleanup-after --no-clean --config-opts=basedir=$MACHINE_ROOT --rootdir=$MOCK_ROOT --define "_without_debug 0" --define "_without_debuginfo 0" --resultdir=RPMS --rebuild --postinstall SRPMS/$KSRPM
check_exit_with_retry "mock -r $MOCK_CONFIG --no-cleanup-after --no-clean --config-opts=basedir=$MACHINE_ROOT --rootdir=$MOCK_ROOT --define "_without_debug 0" --define "_without_debuginfo 0" --resultdir=RPMS --rebuild --postinstall SRPMS/$KSRPM"

mkdir -p $BUILD_ROOT/kernel_rpms/debug
mv $(ls $RPM_ROOT/RPMS/kernel-automotive*) $BUILD_ROOT/kernel_rpms
createrepo_c $BUILD_ROOT/kernel_rpms

}

userspace_compile() {
echo "Start User-Space Compilation"


cd ${BUILD_ROOT}
sh -x ${PATCH_SCRIPTS}/prepare_rpm_source.sh -buildroot ${BUILD_ROOT} -rpmroot rpmbuild -projconf ${PROJ_CONF} -boardconf ${BOARD_CONF}
check_exit "Source compression and RPM creation failed"

cd rpmbuild

for feature in $(cat $LOGS/tarlogs)
do
	FNAME="$(echo $feature | awk -F':' {'print $1'})"
	TARFILE="$(echo $feature | awk -F':' {'print $2'})"
	SPECFILE="$(echo $feature | awk -F':' {'print $3'})"
	BINRPMS="$(echo $feature | awk -F':' {'print $4'} | sed 's|^|RPMS/|g;s|,| RPMS/|g')"
	SHPSTATUS="$(echo $feature | awk -F':' {'print $5'})"

	if [[ "$SHPSTATUS" == "bin_rpm" ]]; then
               mock -r $MOCK_CONFIG --no-cleanup-after --no-clean --config-opts=basedir=$MACHINE_ROOT --rootdir=$MOCK_ROOT --install $BINRPMS
               check_exit "Install RPM Failed for $BINRPMS \nCommand is: mock -r $MOCK_CONFIG --config-opts=basedir=$MACHINE_ROOT --rootdir=$MOCK_ROOT --install $BINRPMS"
               continue
	fi

	FRREL="$(rpmspec -q --qf "%{Release} \n" SPECS/$SPECFILE | head -1 |xargs)"
	FRVER="$(rpmspec -q --qf "%{version} \n" SPECS/$SPECFILE | head -1 |xargs)"
	SOURCERPM="${FNAME}-${FRVER}-${FRREL}.src.rpm"

       	echo -e "\n====Create Binary RPM====\n"
	mock -r $MOCK_CONFIG --no-cleanup-after --no-clean --enable-network --config-opts=basedir=$MACHINE_ROOT --rootdir=$MOCK_ROOT --resultdir=RPMS --rebuild SRPMS/${SOURCERPM} --macro-file=${BOARD_CONF}
	check_exit_with_retry "mock -r $MOCK_CONFIG --no-cleanup-after --no-clean --enable-network --config-opts=basedir=$MACHINE_ROOT --rootdir=$MOCK_ROOT --resultdir=RPMS --rebuild SRPMS/$SOURCERPM --macro-file=${BOARD_CONF}"

	RPMFILES="$(ls RPMS/${FNAME}* | grep -vE "src|source|debug" | xargs)"
        echo -e "\n====Install RPMs====\n"
	mock -r $MOCK_CONFIG --no-cleanup-after --no-clean --enable-network --config-opts=basedir=$MACHINE_ROOT --rootdir=$MOCK_ROOT --install $RPMFILES
	check_exit_with_retry "mock -r $MOCK_CONFIG --config-opts=basedir=$MACHINE_ROOT --rootdir=$MOCK_ROOT --install $RPMFILES"

done

mkdir -p $BUILD_ROOT/userspace-rpms
cp -rf ${RPM_ROOT}/RPMS/* $BUILD_ROOT/userspace-rpms
rm -rf $BUILD_ROOT/userspace-rpms/repodata
createrepo_c $BUILD_ROOT/userspace-rpms

check_exit "createrepo_c failed \ncommand is createrepo_c $BUILD_ROOT/userspace-rpms"
}

compile_simg() {

echo "Start System image Compilation"
echo "Assumption is that we have kernel and usrspace already compiled"


echo "Enter sudo password"
sudo pwd

cd ${BUILD_ROOT}/sample-images
git clean -xdf
git reset --hard HEAD
sudo rm -rf ${BUILD_ROOT}/sample-images/osbuild-manifests/_build

#crearepo fresh for everyone
rm -rf $BUILD_ROOT/userspace-rpms/repodata
rm -rf $BUILD_ROOT/kernel_rpms/repodata
createrepo_c $BUILD_ROOT/kernel_rpms
createrepo_c $BUILD_ROOT/userspace-rpms

cd ${BUILD_ROOT}/sample-images/osbuild-manifests

sed 's,https://buildlogs.centos.org/9-stream/autosd/$arch/packages-main,file://kumartestremove/kernel_rpms,g' -i ${BUILD_ROOT}/sample-images/osbuild-manifests/distro/cs9.ipp.yml
sed "s,kumartestremove,${BUILD_ROOT},g" -i ${BUILD_ROOT}/sample-images/osbuild-manifests/distro/cs9.ipp.yml

sudo dnf clean all
sudo rm -rf /var/tmp/dnf-$USER-*
USRRPMS="$(find $BUILD_ROOT/userspace-rpms -name "*.rpm" |sed 's,.*/,,g'| sed "s,-[1-9].*,,g"|sort -u|xargs | sed 's,^,libbsd ,g'|xargs)"
EXTRPMS=$(echo ["\"$USRRPMS"\"] | sed 's| |","|g')

make cs9-qemu-developer-direct.aarch64.ext4.simg DEFINES='ssh_permit_root_login=true kernel_package="kernel-automotive" kernel_version="5.14.0-999.124ES.test.el9" extra_repos=[{"id":"epel","baseurl":"https://dfw.mirror.rackspace.com/epel/9/Everything/aarch64/"},{"id":"qcom","baseurl":"file:///'"${BUILD_ROOT}"'/userspace-rpms"}] extra_rpms='"${EXTRPMS}"''
check_exit "system image build failed"

cp -rf ${BUILD_ROOT}/sample-images/osbuild-manifests/*.simg ${BUILD_ROOT}/final_images
}


superclean() {
echo "Start Cleanup"
#Cleanup
cd ${BUILD_ROOT}/sample-images
git clean -xdf
git reset --hard HEAD
#change this to remove rpmbuild
echo "Enter sudo password"
sudo rm -rf $RPM_ROOT
rm -rf $BUILD_ROOT/kernel_rpms
sudo rm -rf osbuild-manifests/_build
rm -rf $BUILD_ROOT/userspace-rpms
sudo rm -rf $MACHINE_ROOT
}

clean() {
#Cleanup
echo "Cleanup"
cd ${BUILD_ROOT}/sample-images
git clean -xdf
git reset --hard HEAD
#change this to remove rpmbuild
echo "Enter sudo password"
sudo rm -rf $RPM_ROOT
sudo rm -rf osbuild-manifests/_build
sudo rm -rf $MACHINE_ROOT
}

if [[ "$kernel_flag" == "true" || "$all_flag" == "true" ]]; then
    if [[ -e "$BUILD_ROOT/kernel_rpms" ]]; then
        echo "Starting kernel compilation with existing kernel_rpms"
        
        cd $RPM_ROOT
        KRPMS="$(ls $BUILD_ROOT/kernel_rpms/*.rpm | grep -v '.src.rpm')"
        mock -r $MOCK_CONFIG --no-cleanup-after --no-clean --config-opts=basedir=$MACHINE_ROOT --rootdir=$MOCK_ROOT --install $KRPMS
        check_exit_with_retry "mock -r $MOCK_CONFIG --no-cleanup-after --no-clean --config-opts=basedir=$MACHINE_ROOT --rootdir=$MOCK_ROOT --install $KRPMS"
        cp -rf BUILDROOT/lib/modules/vmlinuz BUILDROOT/lib/modules/dtb/qcom/lemans_vp.dtb $BUILD_ROOT/final_images
        rm -rf $BUILD_ROOT/kernel_rpms/repodata
        createrepo_c $BUILD_ROOT/kernel_rpms
        create_bootimage
    else
        kernel_compile
        create_bootimage
    fi
fi

if [[ "$userspace_flag" == "true" ||  "$all_flag" == "true" ]]; then
    userspace_compile
fi

if [[ "$simg_flag" == "true" ]]; then
    compile_simg
fi

if [[ "$clean" == "true" ]]; then
    clean
fi

if [[ "$superclean" == "true" ]]; then
    superclean
fi


